#include <stdio.h>
#include "hash_table.h"

#include <stdio.h>
#include "dlist.h"

void add(ht_t *table, int data)
{
	int *node;
	node = malloc(sizeof(int));
	*node = data;

	ht_add(table, node);
}

int cmp(void *n1, void *n2)
{
	int i = *(int *)n1, j = *(int *)n2;

	return (i == j) ? (0) : (1);
}

void printer(dlist_node_t *p)
{
	int data = *(int *)dlist_data(p);

	printf(" [%d] ", data);
}

inline void free_node(void *data)
{
	free(data);
}

int hash(void *data)
{
	int number = *(int *)data;
	return number;
}

int main(void)
{
	int ch;
	ht_t *table;
	int number;
	void *key;
	ht_iter_t iter;

	table = ht_create(10, hash, cmp, free);

	while(1) {
		printf("\n1. Insert\n");
		printf("2. Search\n");
		printf("3. Remove\n");
		printf("4. Exit\n\nYour choice ... ");
		fflush(stdin);
		scanf("%d", &ch);
		fflush(stdin);
		scanf("%d", &number);
		switch(ch) {
		case 1:
			add(table, number);
			break;
		case 2:
			key = ht_search(table, &number);
			if(key)
				printf("Number found: %d\n", *(int *)dlist_data(key));
			else
				printf("Not found\n");
			break;
		case 3:
			ht_remove(table, &number);
			break;
		case 4:
			exit(0);
		default:
			printf("Invalid choice\n");
		}
		printf("\nIterating: ");
		ht_iter_init(&iter, table);
		while(ht_iter_data(&iter)) {
			printf(" %d ", *(int *)ht_iter_data(&iter));
			ht_iter_next(&iter);
		}
	}
	return 0;
}
