/**
 * A simple singly linked list implementation
 * - Harshad Shirwadkar(harshad@cmu.edu)
 */

#include <string.h>
#include "list.h"

/** Singly Linked List functions **/
inline list_node_t *list_new_node(void)
{
	list_node_t *node = LIST_ALLOC(sizeof(list_node_t));

	memset(node, 0, sizeof(list_node_t));
	return node;
}


/** create a linked list **/
inline list_node_t *list_create(void)
{
	return list_new_node();
}

/**
 * list_insert_tail:
 * Inserts a node at the tail of list.
 * @args **list:	Double pointer to head of the list
 * @args *data:		Pointer to data
 * @returns	0:		Success
 *			1:		Failure
 */
int list_insert_tail(list_node_t **list, void *data)
{
	list_node_t *p;

	p = (*list);

	if(!p) {
		/* It's an empty list */
		*list = list_create();
		if(!(*list))
			return 1; /* list_create failed to allocate */

		(*list)->data = data;
		return 0;
	}

	while(p->next) {
		p = p->next;
	}

	p->next = list_new_node();
	if(p->next == NULL) {
		/* list_new_node failed to allocate memory */
		return 1;
	}

	p->next->data = data;
	return 0;
}

/* list_next returns the next node. Should be used while iterating
 * through the list */
inline list_node_t *list_next(list_node_t *list)
{
	return (list) ? (list->next) : NULL;
}

/* Fetch the data from the node */
inline void *list_data(list_node_t *node)
{
	return (node) ? (node->data) : NULL;
}

inline void list_free_node(list_node_t *node, void (*rem)(void *))
{
	(*rem)(node->data);
	LIST_FREE(node);
}

/**
 * list_remove_head:
 * Removes head node from the list. Returs 0 on success and 1 on
 * failure.
 */
int list_remove_head(list_node_t **head, void (*rem)(void *))
{
	list_node_t *p;

	if(!head || !*head) {
		return 1;
	}

	p = (*head)->next;
	list_free_node(*head, rem);

	*head = p;
	return 0;
}

/* Initialize list pointer to null */
int list_init(list_node_t **list)
{
	if(!list)
		return 1;
	*list = NULL;

	return 0;
}

void list_dump(list_node_t *list, void (*printer)(list_node_t *))
{
	list_node_t *p = list;

	while(p) {
		(*printer)(p);
		p = list_next(p);
	}
}
