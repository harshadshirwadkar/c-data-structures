#ifndef __LIST_H__
#define __LIST_H__

#include <stdlib.h>

typedef struct list_node {
	void *data;
	struct list_node *next;
} list_node_t;

typedef struct dlist_node {
	void *data;
	struct dlist_node *next, *prev;
} dlist_node_t;

#ifdef __KERNEL__
#define LIST_ALLOC(__s) kmalloc((__s), GFP_KERNEL)
#define LIST_FREE(__p) kfree(__p)
#else
#define LIST_ALLOC(__s) malloc(__s)
#define LIST_FREE(__p) free(__p)
#endif

list_node_t *list_new_node(void);
list_node_t *list_create(void);
int list_insert_tail(list_node_t **, void *);
list_node_t *list_next(list_node_t *);
void *list_data(list_node_t *);
void list_free_node(list_node_t *, void (*rem)(void *));
int list_remove_head(list_node_t **, void (*rem)(void *));
int list_init(list_node_t **);
void list_dump(list_node_t *, void (*printer)(list_node_t *));

#endif /* __LIST_H__ */
