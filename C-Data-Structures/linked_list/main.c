#include <stdio.h>
#include "list.h"

void add(list_node_t **list, int data)
{
	int *node;
	node = malloc(sizeof(int));
	*node = data;

	list_insert_tail(list, node);
}

void printer(list_node_t *p)
{
	int data = *(int *)list_data(p);

	printf(" [%d] \n", data);
}

inline void free_node(void *data)
{
	free(data);
}

int main(void)
{
	list_node_t *list, *iter;

	list_init(&list);

	add(&list, 0);
	add(&list, 0);
	add(&list, 1);
	add(&list, 5);
	add(&list, 2);
	add(&list, 1);
	add(&list, 3);

	list_remove_head(&list, free_node);
	list_remove_head(&list, free_node);

	iter = list;
	while(iter) {
		printf(" %d \n", *((int *)list_data(iter)));
		iter = list_next(iter);
	}
	return 0;
}
