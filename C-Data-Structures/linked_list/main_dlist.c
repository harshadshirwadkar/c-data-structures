#include <stdio.h>
#include "dlist.h"

void add_head(dlist_node_t **list, int data)
{
	int *node;
	node = malloc(sizeof(int));
	*node = data;

	dlist_insert_head(list, node);
}

void add_tail(dlist_node_t **list, int data)
{
	int *node;
	node = malloc(sizeof(int));
	*node = data;

	dlist_insert_tail(list, node);
}

int cmp(void *n1, void *n2)
{
	int i = *(int *)n1, j = *(int *)n2;

	return (i == j) ? (0) : (1);
}

void printer(dlist_node_t *p)
{
	int data = *(int *)dlist_data(p);

	printf(" [%d] ", data);
}

inline void free_node(void *data)
{
	free(data);
}

int main(void)
{
	int ch;
	dlist_node_t *list;
	int number;
	void *key;

	dlist_init(&list);

	while(1) {
		printf("\n1. Insert tail\n");
		printf("2. Insert head\n");
		printf("3. Search\n");
		printf("4. Remove\n");
		printf("5. Exit\n\nYour choice ... ");
		fflush(stdin);
		scanf("%d", &ch);
		fflush(stdin);
		scanf("%d", &number);
		switch(ch) {
		case 1:
			add_tail(&list, number);
			break;
		case 2:
			add_head(&list, number);
			break;
		case 3:
			key = dlist_search(&list, &number, cmp);
			if(key)
				printf("Number found: %d\n", *(int *)dlist_data(key));
			else
				printf("Not found\n");
			break;
		case 4:
			dlist_find_n_remove_node(&list, &number, cmp, free);
			break;
		case 5:
			exit(0);
		default:
			printf("Invalid choice\n");
		}

		dlist_dump(list, printer);
	}
	return 0;
}
